import React from 'react'
import Logo from '../assets/logo.svg'
const HeaderBD = () => {
  return (
    <div className=" mx-auto  ">
    <header className=" bg-[#54925D] rounded-xl py-4 items-center flex justify-between px-2 ">
      <img src={Logo} className='w-20  mr-10 '/>
      <h1 className="text-center text-white font-semibold text-4xl ">
        
        Cargar Base De Datos
        
      </h1>
      <p className='text-white font-semibold text-4xl'>Soil</p>
    </header>
    </div>
  )
}

export default HeaderBD