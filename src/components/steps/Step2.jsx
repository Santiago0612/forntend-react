import React,{ useContext } from "react";
import { StepContext } from "../context/StepContext";
import Fecha from "../Fecha";
import getDate from "../Fecha";
const Step2 = () => {
  const { productorData, setProductorData } = useContext(StepContext);
  const [dateActual, setDateActual] = React.useState("");
  const handleChange = (event) => {
    const { name, value } = event.target;
    setProductorData({ ...productorData, [name]: value });

  };
  return (
    <div className="flex flex-row flex-wrap justify-center gap-y-24 gap-x-20 ">
      <div className=" ">
        <div className=" text-[#54925D] font-bold uppercase">Condicion</div>
        <div className=" p-1 ">
          <input
            className="rounded-xl p-2 px-16 border-2 border-[#54925D]"
            onChange={handleChange}
            value={productorData["condicion"] || ""}
            name="condicion"
            placeholder="Condicion del productor"
          />
        </div>
      </div>
      <div>
        <div className=" text-[#54925D] font-bold uppercase">Departamento</div>
        <div className=" p-1  ">
          <input
            className="rounded-xl p-2 px-16 border-2 border-[#54925D]"
            onChange={handleChange}
            value={productorData["departamento"] || ""}
            name="departamento"
            placeholder="Departamento"
          />
        </div>
      </div>
      <div>
        <div className=" text-[#54925D] font-bold uppercase">Municipio</div>
        <div className=" p-1  ">
          <input
            className="rounded-xl p-2 px-16 border-2 border-[#54925D]"
            onChange={handleChange}
            value={productorData["municipio"] || ""}
            name="municipio"
            placeholder="Municipio"
          />
        </div>
      </div>
      <div>
        <div className=" text-[#54925D] font-bold uppercase">Vereda</div>
        <div className=" p-1  ">
          <input
            className="rounded-xl p-2 px-16 border-2 border-[#54925D]"
            onChange={handleChange}
            value={productorData["vereda"] || ""}
            name="vereda"
            placeholder="Verdeda"
          />
        </div>
      </div>
      <div>
        <div className=" text-[#54925D] font-bold uppercase">Categoria</div>
        <div className=" p-1 ">
        <input
            className="rounded-xl p-2 px-16 border-2 border-[#54925D]"
            name="departamento"
            placeholder="Departamento"
          />
        </div>
      </div>
      <div>
        <div className=" text-[#54925D] font-bold uppercase">
          Fecha de Caracterizacion
        </div>
        <div className=" p-1  ">
        <input
            className="rounded-xl p-2 px-16 border-2 border-[#54925D]"
            value={dateActual}
            onClick={()=>setDateActual(getDate())}
            
          />
        </div>
      </div>
    </div>
  );
};

export default Step2;
