import React from 'react'
import ReactDOM from 'react-dom/client'
import App from './FormRegister.jsx'
import UploadBD from './UploadBD.jsx'
import './index.css'


ReactDOM.createRoot(document.getElementById('root')).render(
  <React.StrictMode>
  <UploadBD />
    
  </React.StrictMode>,
)
