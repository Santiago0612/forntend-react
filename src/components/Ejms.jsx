import React, {useState} from 'react'

const Ejms = () => {
    const [input1, setInput1] = useState("");
    const [input2, setInput2] = useState("");
  
    const handleChange1 = (event) => {
      setInput1(event.target.value);
    };
  
    const handleChange2 = (event) => {
      setInput2(event.target.value);
    };
  
    const handleSubmit = (event) => {
      event.preventDefault();
      console.log(`Input 1: ${input1} | Input 2: ${input2}`);
    };
  
    return (
      <form onSubmit={handleSubmit}>
        <label>
          Input 1:
          <input type="text" value={input1} onChange={handleChange1} />
        </label>
        <label>
          Input 2:
          <input type="text" value={input2} onChange={handleChange2} />
        </label>
        <button type="submit">Enviar</button>
      </form>
    );

}

export default Ejms