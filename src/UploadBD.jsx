
import HeaderBD from "./components/HeaderBD";
import Logo from "./assets/logo.svg";
import * as XLSX from "xlsx";
import { useState } from "react";
import enviarDatos from "./api/api";

const UploadBD = () => {

  const [productorData, setProductorData] = useState([]);

  const save = () => {
    enviarDatos(productorData)
    .then((res) => {
      console.log(res);
    })
    .catch((err) => {
      console.log(err);
      console.log(productorData);
    })
  }

  const readExcel = (file) => {
    const promise = new Promise((resolve, reject) => {
      const fileReader = new FileReader();

      fileReader.readAsArrayBuffer(file);

      fileReader.onload = (e) => {
        const bufferArray = e.target.result;

        const wb = XLSX.read(bufferArray, { type: "buffer" });

        const wsname = wb.SheetNames[0];

        const ws = wb.Sheets[wsname];

        const data = XLSX.utils.sheet_to_json(ws);

        resolve(data);
      };

      fileReader.onerror = (error) => {
        reject(error);
      };
    });
    promise.then((d) => {
      console.log(d);
      setProductorData(d);
    })
  };

  return (
    <div className="bg-[#EDF1D6] h-screen ">
      <HeaderBD />
      <div className="flex justify-center items-center">
        <img
          src={Logo}
          className="w-1/6 mt-[250px] md:hidden sm:hidden  lg:block "
        />
        <div>
          <input
            onChange={(e) => {
              const file = e.target.files[0];
              
              readExcel(file);
            }}
            type="file"
            id="myFile"
            name="filename"
            className="mt-[350px] border-2 border-lime-100 rounded-xl p-4 bg-lime-50"
          />
        </div>
      </div>
      <div className="flex justify-around items-center mt-36">
        <button
          className="bg-white text-slate-400 uppercase py-2 px-4 rounded-xl font-semibold border-2
       bg-[#54925D] hover:bg-[#54925D] hover:text-white transition duration-200 ease-in-out"
        >
          Volver
        </button>
        <button
          onClick={save}
          className="bg-white text-slate-400 uppercase py-2 px-4 rounded-xl font-semibold border-2
       bg-[#54925D] hover:bg-[#54925D] hover:text-white transition duration-200 ease-in-out"
        >
          Guardar
        </button>
      </div>
    </div>
  );
};

export default UploadBD;
