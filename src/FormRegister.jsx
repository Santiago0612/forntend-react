import { useState } from "react";
import StepperControl from "./components/StepperControl";
import Stepper from "./components/Stepper";
import Step1 from "./components/steps/Step1";
import Step2 from "./components/steps/Step2";
import { StepContext } from "./components/context/StepContext";
import enviarDatosAlBackend from "./api/api";
import Header from "./components/Header";

function App() {
  const [currentStep, setCurrentStep] = useState(1);
  const [productorData, setProductorData] = useState("");
  const [finalData, setFinalData] = useState([]);
  const steps = ["Paso 1 ", "Paso 2"];

  const dipalySteps = (step) => {
    switch (step) {
      case 1:
        return <Step1 />;
      case 2:
        return <Step2 />;
    }
  };

  const save = () => {
    enviarDatosAlBackend(productorData)
      .then((res) => {
        console.log(res);
      })
      .catch((err) => {
        console.log(err);
        console.log(productorData);
      });
  };
  const handleClick = (direction) => {
    let newStep = currentStep;

    direction === "next" ? newStep++ : newStep--;
    newStep > 0 && newStep <= steps.length && setCurrentStep(newStep);
  };

  return (
    <>
      <div className="bg-[#EDF1D6] h-screen  ">
        <Header />

        <div className="mx-36 ">
          <div className="container horizontal mt-5">
            <Stepper steps={steps} currentStep={currentStep} />

            <div className="my-10 p-10">
              <StepContext.Provider
                value={{
                  productorData,
                  setProductorData,
                  finalData,
                  setFinalData,
                }}
              >
                {dipalySteps(currentStep)}
              </StepContext.Provider>
            </div>
          </div>
          <StepperControl
            handleClick={handleClick}
            currentStep={currentStep}
            steps={steps}
            onSave={save}
          />
        </div>
      </div>
    </>
  );
}

export default App;
