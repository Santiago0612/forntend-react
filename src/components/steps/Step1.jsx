import { useContext } from "react";
import { StepContext } from "../context/StepContext";



const Step1 = () => {
  const { productorData, setProductorData } = useContext(StepContext);
  const handleChange = (event) => {
    const { name, value } = event.target;
    setProductorData({ ...productorData, [name]: value });
  };

  return (
    <div className="flex flex-row flex-wrap justify-center gap-x-20 gap-y-24  ">
      <div >
        <h1 className=" text-[#54925D] font-bold uppercase ">Nombre</h1>
        <div className=" p-1 ">
          <input
            className="rounded-xl p-2 border-2 border-[#54925D] px-16"
            onChange={handleChange}
            value={productorData["nombre"] || ""}
            name="nombre"
            placeholder="Nombre del productor"
          />
        </div>
      </div>
      <div >
        <div className=" text-[#54925D] font-bold uppercase">Apellido</div>
        <div className=" p-1 flex ">
          <input
            className="rounded-xl p-2 px-16 border-2 border-[#54925D]"
            onChange={handleChange}
            value={productorData["apellido"] || ""}
            name="apellido"
            placeholder="Apellido del productor"
          />
        </div>
      </div>
      <div className>
        <div className=" text-[#54925D] font-bold uppercase">Sexo</div>
        <div className=" p-1 flex ">
          <input
            className="rounded-xl p-2 px-16 border-2 border-[#54925D]"
            onChange={handleChange}
            value={productorData["genero"] || ""}
            name="genero"
            placeholder="Apellido del productor"
          />
        </div>
      </div>
      <div className>
      <div className=" text-[#54925D] font-bold uppercase ">Etnia</div>
        <div className=" p-1 flex ">
            
          <input
            className="rounded-xl p-2 px-16 border-2  border-[#54925D]"
            onChange={handleChange}
            value={productorData["etnia"] || ""}
            name="etnia"
            placeholder="Etnia pérteneciente"
          />
        </div>
      </div>
      <div >
      <div className=" text-[#54925D] font-bold uppercase">Tipo De Documento</div>
        <div className=" p-1 flex ">
            
          <input
            className="rounded-xl p-2 px-16 border-2 border-[#54925D]"
            onChange={handleChange}
            value={productorData["tipo_documento"] || ""}
            name="tipo_documento"
            placeholder="Etnia pérteneciente"
          />
        </div>
      </div>
      <div >
      <div className=" text-[#54925D] font-bold uppercase">Numero De Documento</div>
        <div className=" p-1 flex ">
            
          <input
            className="rounded-xl p-2 px-16 border-2 border-[#54925D]"
            onChange={handleChange}
            value={productorData["numero_documento"] || ""}
            name="numero_documento"
            placeholder="Etnia pérteneciente"
          />
        </div>

      </div>
    </div>
  );
};

export default Step1;
